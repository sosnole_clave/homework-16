﻿#include <iostream>
using namespace std;
class Animal
{
public:
	virtual void Voice() const = 0;
};

class Cat : public Animal
{
public:
	void Voice() const override
	{
		cout << "Meow" << "\n";
	}
};

class Dog : public Animal
{
public:
	void Voice() const override
	{
		cout << "Woof" << "\n";
	}
};

class Bird : public Animal
{
public:
	void Voice() const override
	{
		cout << "Tweet" << "\n";
	}
};

class Mouse : public Animal
{
public:
	void Voice() const override
	{
		cout << "Squeak" << "\n";
	}
};

int main()
{
	Animal* animals[4];
	animals[0] = new Cat;
	animals[1] = new Dog;
	animals[2] = new Bird;
	animals[3] = new Mouse;

	for (int i = 0; i <= 3; i++)
	{
		animals[i]->Voice();
	}

	delete animals[0];
	animals[0] = nullptr;
	delete animals[1];
	animals[1] = nullptr;
	delete animals[2];
	animals[2] = nullptr;
	delete animals[3];
	animals[3] = nullptr;
}